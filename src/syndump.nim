# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[os, parseopt, streams, tables],
  pkg/syndicate,
  pkg/syndicate/relays,
  pkg/syndicate/dataspace_patternizer,
  ./command_line_utils

proc inputPatterns: seq[Pattern] =
  for kind, arg, _ in getopt():
    if kind == cmdArgument:
      try: result.add arg.parsePattern[0]
      except ValueError:
        quit "failed to parse pattern " & arg

proc toLine(values: seq[Value]; prefix: char): string =
  result = newStringOfCap(1024)
  let sep = getEnv("FS", " ")
  result.add(prefix)
  for v in values:
    add(result, sep)
    add(result, $v)
  add(result, '\n')

proc newDumpEntity(turn: Turn): Cap =
  if isJsonModeEnabled():
    let stream = stdout.newFileStream()

    proc m(turn: Turn; v: Value) =
      for e in v.sequence:
        stream.writeText(e, textJson)
      stream.write('\n')
      stream.flush()

    proc a(turn: Turn; v: Value; h: Handle) = m(turn, v)

    newCap(turn.facet, newEntity(turn.facet, publish=a, message=m))

  else:
    var assertions = initTable[Handle, seq[Value]]()

    proc a(turn: Turn; v: Value; h: Handle) =
      var values = v.sequence
      stdout.write(values.toLine('+'))
      stdout.flushFile()
      assertions[h] = values

    proc r(turn: Turn; h: Handle) =
      var values: seq[Value]
      if assertions.pop(h, values):
        stdout.write(values.toLine('-'))
        stdout.flushFile()

    proc m(turn: Turn; v: Value) =
      stdout.write(v.sequence.toLine('!'))
      stdout.flushFile()

    newCap(turn.facet, newEntity(turn.facet, publish=a, retract=r, message=m))

proc main =
  let patterns = inputPatterns()
  runActor("syndex_card") do (turn: Turn):
    let entity = newDumpEntity(turn)
    resolveEnvironment(turn) do (turn: Turn; peer: Cap):
      for pat in patterns:
        discard observe(turn, peer, pat, entity.attenuateFromArgs)

main()
