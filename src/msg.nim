# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[sequtils, os, strutils],
  pkg/syndicate,
  pkg/syndicate/relays

runActor("msg") do (turn: Turn):
  let
    data = map(commandLineParams(), parsePreserves)
    cmd = paramStr(0).extractFilename.normalize
  resolveEnvironment(turn) do (turn: Turn; ds: Cap):
    case cmd
    of "assert":
      for e in data:
        publish(turn, ds, e)
    else: # "msg"
      for e in data:
        message(turn, ds, e)
      sync(turn, ds) do (turn: Turn):
        turn.stopActor()
