# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/parseopt,
  pkg/preserves,
  pkg/syndicate/actors

proc isJsonModeEnabled*: bool =
  ## Check if `-j` or `--json` was passed on the command-line.
  for kind, key, val in getopt():
    if kind == cmdLongOption and key == "json" or
        kind == cmdShortOption and key == "j":
      return true

proc commandLineAttenuations*: seq[Caveat] =
  ## Collect attenuation caveats from the command-line.
  for kind, key, val in getopt():
    if kind == cmdLongOption and key == "attenuate":
      try:
        let pr = val.parsePreserves
        let i = result.len
        result.setLen(succ i)
        if not result[i].fromPreserves pr:
          quit "failed to parse attenuation caveat"
      except ValueError:
        quit "failed to parse attenuation caveat"

proc attenuateFromArgs*(cap: Cap): Cap =
  ## Attenuate a capability by `--attenuate:…` arguments
  ## passed on the command line.
  cap.attenuate commandLineAttenuations()
