# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[parsexml, xmlparser, xmltree],
  pkg/preserves/sugar,
  pkg/preserves/xmlhooks,
  pkg/syndicate,
  ../schema/[assertions, config]

proc translateXml(s: string): XmlTranslation =
  result.xml = s
  try: result.pr = result.xml.parseXml({allowUnquotedAttribs}).toPreservesHook
  except XmlError: discard

proc translatePreserves(pr: Value): XmlTranslation {.gcsafe.} =
  result.pr = pr
  var xn = result.pr.preservesTo(XmlNode)
  if xn.isSome: result.xml = $get(xn)

proc spawnXmlTranslator*(turn: Turn; root: Cap): Facet {.discardable.} =
  turn.withNewFacet:
    result = turn.facet
    during(turn, root, ?:XmlTranslatorArguments) do (ds: Cap):
      linkActor(turn, "xml-translator") do (turn: Turn):
        let xmlPat = observePattern(!XmlTranslation, {@[%0]:grab()})
        during(turn, ds, xmlPat) do (xs: Literal[string]):
          publish(turn, ds, translateXml(xs.value))
        let prPat = observePattern(!XmlTranslation, {@[%1]:grab()})
        during(turn, ds, prPat) do (pr: Literal[Value]):
          publish(turn, ds, translatePreserves(pr.value))

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; ds: Cap):
      spawnXmlTranslator(turn, ds)
