# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[base64, os],
  pkg/nimcrypto/blake2,
  pkg/preserves/sugar,
  pkg/syndicate,
  ../schema/config,
  ../schema/base64 as schema

export Base64DecoderArguments
export schema

proc bootBase64Decoder(turn: Turn; ds: Cap) =
  linkActor(turn, "base64-decoder") do (turn: Turn):
      let decTextPat = observePattern(!Base64Text, { @[%0]: grabLit() })
      during(turn, ds, decTextPat) do (txt: string):
        discard publish(turn, ds, Base64Text(
            txt: txt,
            bin: cast[seq[byte]](decode(txt)),
          ))

      let encTextPat = observePattern(!Base64Text, { @[%1]: grabLit() })
      during(turn, ds, encTextPat) do (bin: seq[byte]):
        discard publish(turn, ds, Base64Text(
            txt: encode(bin),
            bin: bin,
          ))

      let decFilePat = observePattern( !Base64File, { @[%0]: grabLit() })
      during(turn, ds, decFilePat) do (txt: string):
        var
          bin = decode(txt)
          digest = $blake2_256.digest(bin)
          path = getTempDir() / digest
        writeFile(path, bin)
        discard publish(turn, ds, Base64File(
            txt: txt,
            path: path,
            size: bin.len,
          ))

proc spawnBase64Decoder*(turn: Turn; root: Cap): Facet {.discardable.} =
  turn.withNewFacet:
    result = turn.facet
    during(turn, root, ?:Base64DecoderArguments) do (ds: Cap):
      bootBase64Decoder(turn, ds)

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; ds: Cap):
      spawnBase64Decoder(turn, ds)
