# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[monotimes, random, times],
  pkg/syndicate,
  pkg/syndicate/protocols/gatekeeper,
  pkg/syndicate/drivers/timers,
  ../schema/config

const resolution = 0.000_1 # 1 millihertz

type
  PulseEntity {.final.} = ref object of EntityObj
    ## Entity that receives timer messages and
    ## asserts and retracts.
    self, driver: Cap
    detail: PulseDetail
    assertHandle, timerHandle: Handle
    rng: Rand
    timeoutTime: MonoTime
      # sloppy drift accounting

proc scheduleFlip(pulse: PulseEntity; turn: Turn; flipOn: bool; drift: float) =
  var
    now = getMonotime()
    cyclePeriod, timeout: float
  while cyclePeriod <= resolution:
    cyclePeriod = pulse.rng.gauss(mu = pulse.detail.frequency, sigma = pulse.detail.dither)
  while timeout <= resolution:
    if flipOn:
      timeout = (cyclePeriod - cyclePeriod * pulse.detail.`duty-factor`) - drift
    else:
      timeout = cyclePeriod * pulse.detail.`duty-factor` - drift
  var dur = initDuration(microseconds = int(timeout * 1_000_000.0))
  pulse.timeoutTime = now + dur
  replace(turn, pulse.driver, pulse.timerHandle, SetTimer(
      label: flipOn.toPreserves,
      seconds: timeout - (resolution * 0.75),
      kind: TimerKind.relative,
      peer: pulse.self.embed,
    ))

proc flip(pulse: PulseEntity; turn: Turn; state: bool) =
  if state:
    assert pulse.assertHandle == 0.Handle
    pulse.assertHandle = publish(turn, pulse.detail.target.Cap, pulse.detail.assertion)
  elif pulse.assertHandle != 0.Handle:
    retract(turn, pulse.assertHandle)
    pulse.assertHandle = 0.Handle

proc pulseMessage(turn: Turn; e: Entity; v: Value) =
  let pulse = PulseEntity(e)
  var now = getMonotime()
  var exp: TimerExpired
  if exp.fromPreserves(v):
    var drift = (now - pulse.timeoutTime).inMicroseconds.float * 0.000_000_1
    pulse.scheduleFlip(turn, exp.label.isFalse, drift)
    pulse.flip(turn, not exp.label.isFalse)

proc newPulseEntity(turn: Turn; detail: PulseDetail; timerDriver: Cap): PulseEntity =
  if not (detail.target of Cap):
    raise newException(ValueError, "pulse target is not an embedded Cap")
  result = PulseEntity(
      facet: turn.facet,
      m: pulseMessage,
      driver: timerDriver,
      detail: detail,
      rng: initRand(),
    )
  result.self = result.newCap()

proc spawnPulseActor*(turn: Turn; core, relay: Cap): Facet {.discardable.} =
  ## The `core: Cap` argument must be a dataspace with a timer driver.
  turn.withNewFacet:
    result = turn.facet
    let resolvePat = Resolve?:{ 0: matchRecord("pulse", grab()), 1: grab() }
    during(turn, relay, resolvePat) do (v: Value; observer: Cap):
      linkActor(turn, "pulse") do (turn: Turn):
          var detail: PulseDetail
          if not detail.fromPreserves(v) or
              detail.frequency < 0.000_1 or
              detail.`duty-factor` < 0.0 or
              detail.`duty-factor` > 1.0 or
              detail.dither < 0.0 or
              detail.dither > detail.frequency:
            var r = Resolved(orKind: ResolvedKind.Rejected)
            r.rejected.detail = "invalid pulse parameters".toPreserves
            discard publish(turn, observer, r)
          else:
            let pulse = turn.newPulseEntity(detail, core)
            pulse.scheduleFlip(turn, true, 0.0)
            publish(turn, observer, ResolvedAccepted(
              responderSession: pulse.newCap))

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; relay: Cap):
      let core = turn.facet.newDataspace()
      spawnTimerDriver(turn, core)
      spawnPulseActor(turn, core, relay)
