# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/json,
  pkg/sys/[ioqueue, sockets],
  pkg/preserves/jsonhooks,
  pkg/syndicate,
  pkg/syndicate/protocols/gatekeeper,
   ../schema/[config, json_messages]

type
  AsyncJsonConn = AsyncConn[sockets.Protocol.TCP] | AsyncConn[sockets.Protocol.Unix]

  JsonTranslator = TcpTranslator | UnixTranslator

  TcpTranslator {.final.} = ref object of EntityObj
    sock: AsyncConn[sockets.Protocol.TCP]
    ds: Cap
    sa: TcpAddress

  UnixTranslator {.final.} = ref object of EntityObj
    sock: AsyncConn[sockets.Protocol.Unix]
    ds: Cap
    sa: UnixAddress

proc init(turn: Turn; t: JsonTranslator) =
  t.facet = turn.facet
  t.facet.onStop do (turn: Turn):
    t.sock.close()
  t.ds = t.facet.newDataspace()
  observe(turn, t.ds, matchRecord("send".toSymbol, grab()), turn.facet.newCap(t))

proc recv(t: JsonTranslator; items: sink seq[Value]) =
  queueTurn(t.facet, externalCause "recv") do (turn: Turn):
    for e in items:
      message(turn, t.ds, initRecord("recv", e))

template loopBody {.dirty.} =
  # Template workaround for CPS and parameterized types.
  var dec = newBufferedDecoder(0)
  let buf = new string # TODO: get a pointer into the decoder?
  buf[].setLen(0x4000)
  while t.facet.isActive:
    # TODO: parse buffer
    let n = read(t.sock, buf)
    if n < 1:
      queueTurn(t.facet, externalCause "read error", stopActor)
      break
    else:
      dec.feed(buf[][0].addr, n)
      var
        items: seq[Value]
        data = dec.parse()
      while data.isSome:
        items.add data.get
        data = dec.parse()
      if items.len > 0: t.recv(items)

proc loop(t: TcpTranslator) {.asyncio.} =
  loopBody()

proc loop(t: UnixTranslator) {.asyncio.} =
  loopBody()

proc translate(t: JsonTranslator; observer: Cap) =
  queueTurn(t.facet, externalCause "connected") do (turn: Turn):
    init(turn, t)
    publish(turn, observer, ResolvedAccepted(responderSession: t.ds))
      # Resolve the <json-socket-translator { }> step.
    discard trampoline:
      whelp t.loop()

proc publishConnectError(facet: Facet; observer: Cap; err: ref Exception) =
  queueTurn(facet, externalCause "connection error") do (turn: Turn):
    publish(turn, observer, Rejected(detail: err.msg.toPreserves))

proc mTcp(turn: Turn; e: Entity; v: Value) =
  discard trampoline:
    whelp TcpTranslator(e).sock.write(v[0].jsonText & "\n")

proc mUnix(turn: Turn; e: Entity; v: Value) =
  discard trampoline:
    whelp UnixTranslator(e).sock.write(v[0].jsonText & "\n")

proc translate(facet: Facet; sa: SocketAddress; observer: Cap) {.asyncio.} =
  try:
    case sa.orKind
    of SocketAddressKind.TcpAddress:
      TcpTranslator(
          facet: facet, m: mTcp,
          sock: connectTcpAsync(sa.tcpaddress.host, Port sa.tcpaddress.port),
          sa: sa.tcpaddress,
        ).translate(observer)
    of SocketAddressKind.UnixAddress:
      UnixTranslator(
          facet: facet, m: mUnix,
          sock: connectUnixAsync(sa.unixaddress.path),
          sa: sa.unixaddress,
        ).translate(observer)
  except CatchableError as err:
    publishConnectError(facet, observer, err)

proc gatekeepJsonSocketTranslator*(turn: Turn; relay: Cap) =
  let pat = Resolve.match({ 0: JsonSocketTranslatorStep.grabTypeFlat(), 1: grab() })
  during(turn, relay, pat) do (sockAddr: SocketAddress, observer: Cap):
    linkActor(turn, "json-socket-translator") do (turn: Turn):
      turn.facet.preventInertCheck() # Actor will be inert while connecting.
      discard trampoline:
        whelp translate(turn.facet, sockAddr, observer)

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; relay: Cap):
      gatekeepJsonSocketTranslator(turn, relay)
