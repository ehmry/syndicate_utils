# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[json, osproc],
  pkg/preserves/jsonhooks,
  pkg/syndicate,
  ../schema/[config, json_messages]

proc runChild(params: seq[string]): string =
  if params.len < 1:
    stderr.writeLine "not enough parameters"
  let
    cmd = params[0]
    args = params[1..params.high]
  try: result = execProcess(command=cmd, args=args, options={poUsePath})
  except CatchableError as err:
    quit("execProcess failed: " & err.msg)
  if result == "":
    stderr.writeLine "no ouput"

proc spawnJsonStdioTranslator*(turn: Turn; root: Cap): Facet {.discardable.} =
  turn.withNewFacet:
    result = turn.facet
    during(turn, root, ?:JsonTranslatorArguments) do (argv: seq[string], ds: Cap):
      linkActor(turn, "json-stdio-translator") do (turn: Turn):
        var js = parseJson(runChild(argv))
        message(turn, ds,  RecvJson(data: js))
        publish(turn, ds,  RecvJson(data: js))

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; ds: Cap):
      spawnJsonStdioTranslator(turn, ds)
