# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[os, oserrors, posix, syncio],
  pkg/sys/[files, ioqueue],
  pkg/syndicate,
  pkg/syndicate/protocols/gatekeeper

import ../schema/[config, file_system]

from pkg/sys/handles import FD

proc stopForOsError(turn: Turn; cap: Cap) =
  message(turn, cap, initRecord("error", osLastError().osErrorMsg().toPreserves))
  turn.stopFacet()

proc stopAsOkay(turn: Turn; cap: Cap) =
  message(turn, cap, initRecord"ok")
  turn.stopFacet()

const iounit =  0x1000

type Buffer = ref seq[byte]

proc newBuffer(n: int): Buffer =
  new result
  if n < 0: result[].setLen iounit
  else: result[].setLen min(n, iounit)

proc read(facet: Facet; file: AsyncFile; count: BiggestInt; buf: Buffer; dst: Cap)

proc readAsync(facet: Facet; file: AsyncFile; count: BiggestInt; buf: Buffer; dst: Cap) {.asyncio.} =
  # TODO: optimise
  assert count != 0
  let n = file.read(buf)
  proc deliver(turn: Turn) {.closure.} =
    case n
    of -1:
      turn.stopForOsError(dst)
    else:
      if n < buf[].len:
        buf[].setLen(n)
        if n > 0:
          message(turn, dst, initRecord("buf", buf[].toPreserves))
        turn.stopAsOkay(dst)
      else:
        message(turn, dst, buf[])
        var count = count
        if count != -1:
          count = count - n
        read(facet, file, count, buf, dst)
  queueTurn(facet, externalCause "async read", deliver)

proc read(facet: Facet; file: AsyncFile; count: BiggestInt; buf: Buffer; dst: Cap) =
  discard trampoline:
    whelp readAsync(facet, file, count, buf, dst)

proc read(facet: Facet; file: AsyncFile; count: BiggestInt; dst: Cap) =
  ## Call read with a reuseable buffer.
  read(facet, file, count, newBuffer(count.int), dst)

proc serve(turn: Turn; detail: FileSystemDetail; ds: Cap) =
  detail.root.createDir()

  during(turn, ds, Read.grabType) do (op: Read):
    let dst = op.sink.Cap
    let fd = posix.open(detail.root / op.path, O_RDONLY or O_NONBLOCK, 0)
    if fd < 0:
      turn.stopForOsError(dst)
    else:
      if op.count == 0:
        discard close(fd)
        message(turn, dst, initRecord"ok")
        turn.stopFacet()
      elif posix.lseek(fd, op.offset, SEEK_SET) < 0:
        discard close(fd)
        turn.stopForOsError(dst)
      else:
        # fd is hopefully automagically closed.
        turn.facet.read(fd.FD.newAsyncFile, op.count, dst)

  let contentObserve = observePattern(FileContent.matchType, {0:grab(), 1:grab()})
  during(turn, ds, contentObserve) do (path: Literal[string], mode: Literal[Symbol]):
    var content = FileContent(path: path.value, mode: mode.value)
    let fullPath = detail.root / content.path
    if fullPath.fileExists:
      case content.mode.string
      of "bytes":
        content.data = cast[seq[byte]](readFile(fullPath)).toPreserves
      else:
        content.data = initRecord("error", "unknown file-content mode".toPreserves)
    discard publish(turn, ds, content)

  onMessage(turn, ds, WriteText.grabWithin) do (path: string, text: string):
    let fullPath = detail.root / path
    fullPath.writeFile(text)

  onMessage(turn, ds, RemoveFile.grabWithin) do (path: string):
    let fullPath = detail.root / path
    fullPath.removeFile()

proc spawnFileSystemActor*(turn: Turn; relay: Cap): Facet {.discardable.} =
  turn.withNewFacet:
    result = turn.facet
    let resolvePat = Resolve?:{ 0: FileSystemStep.grabWithin, 1: grab() }
    during(turn, relay, resolvePat) do (detail: FileSystemDetail; observer: Cap):
      linkActor(turn, "file-system") do (turn: Turn):
          let ds = turn.facet.newDataspace()
          serve(turn, detail, ds)
          discard publish(turn, observer, ResolvedAccepted(responderSession: ds))

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; relay: Cap):
      spawnFileSystemActor(turn, relay)
