# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/times,
  pkg/syndicate,
  pkg/syndicate/durings,
  pkg/syndicate/drivers/timers

import ../schema/config

proc afterTimeout(n: float64): LaterThan =
  ## Get a `LaterThan` record for `n` seconds in the future.
  result.seconds = getTime().toUnixFloat() + n

type CacheEntity {.final.} = ref object of EntityObj
  timeouts, target: Cap
    # dataspaces for observing timeouts and publishing values
  pattern: Pattern
  lifetime: float64

proc cachePublish(turn: Turn; e: Entity; ass: Value; h: Handle) =
  ## Re-assert pattern captures in a sub-facet.
  let cache = CacheEntity(e)
  turn.withNewfacet:
    # TODO: a seperate facet for every assertion, too much?
    let ass = depattern(cache.pattern, ass.sequence)
      # Build an assertion with what he have of the pattern and capture.
    discard publish(turn, cache.target, ass)
    let timeout = afterTimeout(cache.lifetime)
    onPublish(turn, cache.timeouts, ?timeout) do:
      turn.stopFacet() # end this facet

proc isObserve(pat: Pattern): bool =
  pat.orKind == PatternKind.group and
    pat.group.type.orKind == GroupTypeKind.rec and
      pat.group.type.rec.label.isSymbol"Observe"

proc spawnCacheActor*(turn: Turn; core, relay: Cap): Facet {.discardable.} =
  turn.withNewFacet:
    result = turn.facet
    during(turn, relay, ?:CacheArguments) do (ds: Cap, lifetime: float64):
      linkActor(turn, "cache_actor") do (turn: Turn):
        onPublish(turn, ds, ?:Observe) do (pat: Pattern, obs: Cap):
          var cache: CacheEntity
          if obs.relay != turn.facet and not(pat.isObserve):
            # Watch pattern if the observer is not us
            # and if the pattern isn't a recursive observe
            cache = CacheEntity(
                facet: turn.facet,
                a: cachePublish,
                timeouts: core,
                target: ds,
                pattern: pat,
                lifetime: lifetime,
              )
            discard observe(turn, ds, pat, cache.newCap())

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; relay: Cap):
      let core = turn.facet.newDataspace()
      spawnTimerDriver(turn, core)
      spawnCacheActor(turn, core, relay)
