# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[dirs, os, paths],
  pkg/preserves/sugar,
  pkg/syndicate

import ../schema/[assertions, config]

proc spawnFileSystemUsageActor*(turn: Turn; root: Cap): Facet {.discardable.} =
  turn.withNewFacet:
    result = turn.facet
    during(turn, root, ?:FileSystemUsageArguments) do (ds: Cap):
      let pat = observePattern(!FileSystemUsage, { @[%0]: grab() })
      during(turn, ds, pat) do (lit: Literal[string]):
        linkActor(turn, "file-system-usage") do (turn: Turn):
          var ass = FileSystemUsage(path: lit.value)
          if fileExists(ass.path): ass.size = getFileSize(ass.path)
          else:
            for fp in walkDirRec(paths.Path(lit.value), yieldFilter={pcFile}):
              var fs = getFileSize(string fp)
              inc(ass.size, fs)
          discard publish(turn, ds, ass)
            # TODO: updates?

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; ds: Cap):
      spawnFileSystemUsageActor(turn, ds)
