# SPDX-FileCopyrightText: ☭ 2022 Emery Hemingway
# SPDX-License-Identifier: Unlicense

## See the rofi-script(5) manpage for documentation.

import std/[cmdline, envvars, strutils, tables]
import preserves, syndicate, syndicate/relays
import ./schema/rofi

if getEnv("ROFI_OUTSIDE") == "":
  quit("run this program in rofi")

runActor("rofi_script_actor") do (turn: Turn):
  resolveEnvironment(turn) do (turn: Turn; ds: Cap):
    case paramCount()
    of 0:
      let pat = ?:Options
      onPublish(turn, ds, pat) do (options: seq[string]):
        stdout.writeLine options.join("\n")
        quit()

    of 1:
      var select = Select(option: commandLineParams()[0])
      for (key, val) in envPairs():
        if key.startsWith "ROFI_":
          select.environment[Symbol key] = val
      message(turn, ds, select)
      sync(turn, ds, stopActor)

    else:
      quit("rofi passed an unexpected number of arguments")
