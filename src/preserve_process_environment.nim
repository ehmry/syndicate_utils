# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/[tables, os]
import preserves

type ProcessInfo {.preservesDictionary.} = object
  program: string
  argv: seq[string]
  env: Table[string, string]
  dir: string

proc main =
  var info: ProcessInfo
  info.program = getAppFilename()
  info.argv = commandLineParams()
  for key, val in envPairs(): info.env[key] = val
  info.dir = getCurrentDir()
  writeLine(stdout, info.toPreserves)

main()
