# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

# TODO: link this as a sub-command of something else.

import
  std/[parseopt, streams],
  pkg/syndicate,
  pkg/syndicate/protocols/rpc,
  pkg/syndicate/relays,
  ./command_line_utils

runActor("synqa") do (turn: Turn):
  resolveEnvironment(turn) do (turn: Turn; ds: Cap):
    let
      caveats = commandLineAttenuations()
      output = stdout.newFileStream()
      mode = if isJsonModeEnabled(): textJson else: textPreserves
    for kind, arg, _ in getopt():
      if kind == cmdArgument:
        var request: Value
        try: request = arg.parsePreserves
        except: quit("failed to parse Preserves " & arg)
        publish(turn, ds, Question(request: request))
        onPublish(turn, ds, Answer.match { 0: request.match, 1: grab() }) do (response: Value):
          # var v = response
          for c in caveats:
            response = attenuate(response, c)
          output.writeText(response, mode)
          output.write('\n')
          output.flush()
