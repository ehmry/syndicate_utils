# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  pkg/getdns,
  pkg/getdns/events_sys,
  pkg/preserves/sugar,
  pkg/syndicate,
  pkg/syndicate/protocols/gatekeeper,
  ./schema/[config, dns_records]

type
  StrLit = Literal[string]
  CallbackContext = ref CallbackContextObj | ptr CallbackContextObj
  CallbackContextObj[T] = object
    facet: Facet
    ds: Cap
    ctx: getdns.Context
    q: T

proc clear(cb: CallbackContext) =
  if not cb.ctx.isNil:
    cb.ctx.destroy()

proc newCallbackContext[T](facet: Facet; ds: Cap; query: T): ref CallbackContextObj[T] =
  new result
  result.facet = facet
  result.ds = ds
  block:
    result.ctx = getdns.newContext(true)
    result.ctx.set_resolution_type(GETDNS_RESOLUTION_STUB)
    doAssert result.ctx.set_sys_ioqueue_base().isGood
  result.q = query
  let cb = result
  facet.onStop do (turn: Turn):
    cb.clear()
  facet.preventInertCheck()

proc publishError(turn: Turn; cb: CallbackContext; err: string) =
  assert not cb.isNil
  publish(turn, cb.ds, initRecord("a", cb.q.toPreserves, initRecord("error", err.toPreserves)))

proc serviceCallback(
    context: Context;
    callback_type: getdns_callback_type_t;
    response: Dict;
    userarg: pointer;
    txid: Transaction) {.cdecl.} =
  let cb = cast[ptr CallbackContextObj[SrvQ]](userarg)
  queueTurn(cb.facet, externalCause "getdns") do (turn: Turn):
    if callback_type != GETDNS_CALLBACK_COMPLETE:
      publishError(turn, cb, callback_type.cint.errorString)
    elif response{"status"}.int != RESPSTATUS_GOOD:
      publishError(turn, cb, $response)
    else:
       for reply in response{"replies_tree"}:
        for answer in reply{"answer"}:
          let rdata = answer{"rdata"}
          publish(turn, cb.ds, initRecord("a", cb.q.toPreserves, SrvA(
              target: rdata{"target"}.bindata.toFqdn,
              port: rdata{"port"}.int.BiggestInt,
              priority: rdata{"priority"}.int.BiggestInt,
              weight: rdata{"weight"}.int.BiggestInt)))
    cb.clear()

proc serve(turn: Turn; ds: Cap) =
  during(turn, ds, matchRecord("q", SrvQ.matchType.grab)) do (srvq: SrvQ):
    var
      extensions: Dict
      txid: Transaction
    let
      cb = newCallbackContext(turn.facet, ds, srvq)
      r = cb.ctx.service(
        srvq.name,
        extensions,
        userarg=cb[].addr,
        txid.addr,
        serviceCallback)
    if r.isBad:
      publishError(turn, cb, r.cint.errorString)
      cb.clear()

  let patObserveSrv = Srv.matchType.observePattern { @[%0]:grab() }
  during(turn, ds, patObserveSrv) do (name: StrLit):
    let query = SrvQ(name: name.value).toPreserves
    publish(turn, ds, initRecord("q", query))
    let patA = matchRecord("a", query.match, grab())
    onPublish(turn, ds, patA) do (a: SrvA):
      publish(turn, ds, Srv(
          name: name.value,
          priority: a.priority,
          weight: a.weight,
          port: a.port,
          target: a.target))

  #[
  let patTxt = Txt.matchType.observePattern { @[%0]:grab() }
  during(turn, ds, patTxt) do (owner: StrLit):
    var
      response, extensions: getdns.Dict
      context = getdns.newContext(true)
    context.setResolutionType(GETDNS_RESOLUTION_STUB)
    let r = context.general_sync(owner.value, RRTYPE_TXT, extensions, addr response)
    if r.isBad:
      echo "general_sync failed for TXT ", owner.value, ": ", $r
    elif response["status"].int != RESPSTATUS_GOOD:
      echo "general_sync status not good for TXT ", owner.value, ": ", $response["status"]
    else:
      for reply in response["replies_tree"]:
        for answer in reply["answer"]:
          for data in answer["rdata"]["txt_strings"]:
            publish(turn, ds, Txt(name: owner.value, data: $data.bindata))
  ]#

proc spawnDnsResolver(turn: Turn; relay: Cap) =
  let pat = Resolve?:{ 0: DnsResolverStep.matchType, 1: grab() }
  during(turn, relay, pat) do (observer: Cap):
    linkActor(turn, "getdns-resolver") do (turn: Turn):
      let ds = turn.facet.newDataspace()
      serve(turn, ds)
      publish(turn, observer, ResolvedAccepted(responderSession: ds))

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; relay: Cap):
      spawnDnsResolver(turn, relay)

# TODO: refresh assertions after TTL expires.
