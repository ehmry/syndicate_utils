
import
  preserves, std/tables

type
  Select* {.preservesRecord: "rofi-select".} = object
    `option`*: string
    `environment`*: Environment
  Environment* = Table[Symbol, string]
  Options* {.preservesRecord: "rofi-options".} = object
    `options`*: seq[string]
proc `$`*(x: Select | Environment | Options): string =
  `$`(toPreserves(x))

proc encode*(x: Select | Environment | Options): seq[byte] =
  encode(toPreserves(x))
