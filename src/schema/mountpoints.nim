
import
  preserves

type
  StatusKind* {.pure.} = enum
    `Failure`, `success`
  `Status`* {.preservesOr.} = object
    case orKind*: StatusKind
    of StatusKind.`Failure`:
      `failure`*: Failure
    of StatusKind.`success`:
      `success`* {.preservesLiteral: "#t".}: bool
  Mountpoint* {.preservesRecord: "mount".} = object
    `source`*: string
    `target`*: string
    `type`*: string
    `status`*: Status
  Failure* {.preservesRecord: "failure".} = object
    `msg`*: string
proc `$`*(x: Status | Mountpoint | Failure): string =
  `$`(toPreserves(x))

proc encode*(x: Status | Mountpoint | Failure): seq[byte] =
  encode(toPreserves(x))
