
import
  preserves

type
  XsltItem* = string
  Pulse* {.preservesRecord: "pulse".} = object
    `periodSec`*: float
    `proxy`* {.preservesEmbedded.}: Value
  XsltTransform* {.preservesRecord: "xslt-transform".} = object
    `stylesheet`*: string
    `input`*: string
    `output`*: Value
  XsltItems* = seq[XsltItem]
  XmlTranslation* {.preservesRecord: "xml-translation".} = object
    `xml`*: string
    `pr`*: Value
  FileSystemUsage* {.preservesRecord: "file-system-usage".} = object
    `path`*: string
    `size`*: BiggestInt
proc `$`*(x: XsltItem | Pulse | XsltTransform | XsltItems | XmlTranslation |
    FileSystemUsage): string =
  `$`(toPreserves(x))

proc encode*(x: XsltItem | Pulse | XsltTransform | XsltItems | XmlTranslation |
    FileSystemUsage): seq[byte] =
  encode(toPreserves(x))
