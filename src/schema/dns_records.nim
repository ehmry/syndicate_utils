
import
  preserves

type
  SrvQ* {.preservesRecord: "srv".} = object
    `name`*: string
  TxtA* {.preservesRecord: "ok".} = object
    `data`*: string
  Srv* {.preservesRecord: "srv".} = object
    `name`*: string
    `priority`*: BiggestInt
    `weight`*: BiggestInt
    `port`*: BiggestInt
    `target`*: string
  TxtQ* {.preservesRecord: "txt".} = object
    `name`*: string
  Txt* {.preservesRecord: "txt".} = object
    `name`*: string
    `data`*: string
  SrvA* {.preservesRecord: "ok".} = object
    `target`*: string
    `port`*: BiggestInt
    `priority`*: BiggestInt
    `weight`*: BiggestInt
proc `$`*(x: SrvQ | TxtA | Srv | TxtQ | Txt | SrvA): string =
  `$`(toPreserves(x))

proc encode*(x: SrvQ | TxtA | Srv | TxtQ | Txt | SrvA): seq[byte] =
  encode(toPreserves(x))
