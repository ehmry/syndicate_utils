
import
  preserves

type
  QueryQuestion* {.preservesRecord: "q".} = object
    `query`*: Query
  SqlError* {.preservesRecord: "error".} = object
    `msg`*: string
    `context`*: string
  QueryAnswer* {.preservesRecord: "a".} = object
    `query`*: Query
    `answer`*: Value
  Query* {.preservesRecord: "sql".} = object
    `statement`* {.preservesTupleTail.}: seq[Value]
proc `$`*(x: QueryQuestion | SqlError | QueryAnswer | Query): string =
  `$`(toPreserves(x))

proc encode*(x: QueryQuestion | SqlError | QueryAnswer | Query): seq[byte] =
  encode(toPreserves(x))
