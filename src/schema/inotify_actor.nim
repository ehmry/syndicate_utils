
import
  preserves

type
  InotifyMessage* {.preservesRecord: "inotify".} = object
    `path`*: string
    `event`*: Symbol
    `cookie`*: BiggestInt
    `name`*: string
proc `$`*(x: InotifyMessage): string =
  `$`(toPreserves(x))

proc encode*(x: InotifyMessage): seq[byte] =
  encode(toPreserves(x))
