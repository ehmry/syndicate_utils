
import
  preserves

type
  Read* {.preservesRecord: "read".} = object
    `path`*: string
    `offset`*: BiggestInt
    `count`*: BiggestInt
    `sink`* {.preservesEmbedded.}: EmbeddedRef
  WriteText* {.preservesRecord: "write-text".} = object
    `path`*: string
    `text`*: string
  FileContent* {.preservesRecord: "file-content".} = object
    `path`*: string
    `mode`*: Symbol
    `data`*: Value
  RemoveFile* {.preservesRecord: "remove-file".} = object
    `path`*: string
  Buffer* {.preservesRecord: "buf".} = object
    `data`*: seq[byte]
proc `$`*(x: Read | WriteText | FileContent | RemoveFile | Buffer): string =
  `$`(toPreserves(x))

proc encode*(x: Read | WriteText | FileContent | RemoveFile | Buffer): seq[byte] =
  encode(toPreserves(x))
