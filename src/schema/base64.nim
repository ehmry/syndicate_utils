
import
  preserves

type
  Base64File* {.preservesRecord: "base64-file".} = object
    `txt`*: string
    `path`*: string
    `size`*: BiggestInt
  Base64Text* {.preservesRecord: "base64".} = object
    `txt`*: string
    `bin`*: seq[byte]
proc `$`*(x: Base64File | Base64Text): string =
  `$`(toPreserves(x))

proc encode*(x: Base64File | Base64Text): seq[byte] =
  encode(toPreserves(x))
