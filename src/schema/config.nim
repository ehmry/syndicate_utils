
import
  preserves

type
  Tcp* {.preservesRecord: "tcp".} = object
    `host`*: string
    `port`*: BiggestInt
  DnsResolverDetail* {.preservesDictionary.} = object
  XmlTranslatorArgumentsField0* {.preservesDictionary.} = object
    `dataspace`* {.preservesEmbedded.}: EmbeddedRef
  XmlTranslatorArguments* {.preservesRecord: "xml-translator".} = object
    `field0`*: XmlTranslatorArgumentsField0
  PostgreStepField0* {.preservesDictionary.} = object
    `connection`*: seq[PostgreConnectionParameter]
  PostgreStep* {.preservesRecord: "postgre".} = object
    `field0`*: PostgreStepField0
  TcpAddress* {.preservesRecord: "tcp".} = object
    `host`*: string
    `port`*: BiggestInt
  XsltArgumentsField0* {.preservesDictionary.} = object
    `dataspace`* {.preservesEmbedded.}: EmbeddedRef
  XsltArguments* {.preservesRecord: "xslt".} = object
    `field0`*: XsltArgumentsField0
  JsonSocketTranslatorStepDetail* {.preservesDictionary.} = object
    `socket`*: SocketAddress
  PulseDetail* {.preservesDictionary.} = object
    `assertion`*: Value
    `dither`*: float
    `duty-factor`*: float
    `frequency`*: float
    `target`* {.preservesEmbedded.}: EmbeddedRef
  Base64DecoderArgumentsField0* {.preservesDictionary.} = object
    `dataspace`* {.preservesEmbedded.}: EmbeddedRef
  Base64DecoderArguments* {.preservesRecord: "base64-decoder".} = object
    `field0`*: Base64DecoderArgumentsField0
  FileSystemDetail* {.preservesDictionary.} = object
    `root`*: string
  PrinterStepField0* {.preservesDictionary.} = object
  PrinterStep* {.preservesRecord: "printer".} = object
    `field0`*: PrinterStepField0
  FileSystemStep* {.preservesRecord: "file-system".} = object
    `detail`*: FileSystemDetail
  CacheArgumentsField0* {.preservesDictionary.} = object
    `dataspace`* {.preservesEmbedded.}: EmbeddedRef
    `lifetime`*: float
  CacheArguments* {.preservesRecord: "cache".} = object
    `field0`*: CacheArgumentsField0
  JsonSocketTranslatorStep* {.preservesRecord: "json-socket-translator".} = object
    `detail`*: JsonSocketTranslatorStepDetail
  SocketAddressKind* {.pure.} = enum
    `TcpAddress`, `UnixAddress`
  `SocketAddress`* {.preservesOr.} = object
    case orKind*: SocketAddressKind
    of SocketAddressKind.`TcpAddress`:
      `tcpaddress`*: TcpAddress
    of SocketAddressKind.`UnixAddress`:
      `unixaddress`*: UnixAddress
  SqliteStepField0* {.preservesDictionary.} = object
    `database`*: string
  SqliteStep* {.preservesRecord: "sqlite".} = object
    `field0`*: SqliteStepField0
  DnsResolverStep* {.preservesRecord: "dns-resolver".} = object
    `detail`*: DnsResolverDetail
  PulseStep* {.preservesRecord: "pulse".} = object
    `detail`*: PulseDetail
  JsonTranslatorArgumentsField0* {.preservesDictionary.} = object
    `argv`*: seq[string]
    `dataspace`* {.preservesEmbedded.}: EmbeddedRef
  JsonTranslatorArguments* {.preservesRecord: "json-stdio-translator".} = object
    `field0`*: JsonTranslatorArgumentsField0
  UnixAddress* {.preservesRecord: "unix".} = object
    `path`*: string
  PostgreConnectionParameter* {.preservesTuple.} = object
    `key`*: string
    `val`*: string
  FileSystemUsageArgumentsField0* {.preservesDictionary.} = object
    `dataspace`* {.preservesEmbedded.}: EmbeddedRef
  FileSystemUsageArguments* {.preservesRecord: "file-system-usage".} = object
    `field0`*: FileSystemUsageArgumentsField0
proc `$`*(x: Tcp | DnsResolverDetail | XmlTranslatorArguments | PostgreStep |
    TcpAddress |
    XsltArguments |
    JsonSocketTranslatorStepDetail |
    PulseDetail |
    Base64DecoderArguments |
    FileSystemDetail |
    PrinterStep |
    FileSystemStep |
    CacheArguments |
    JsonSocketTranslatorStep |
    SocketAddress |
    SqliteStep |
    DnsResolverStep |
    PulseStep |
    JsonTranslatorArguments |
    UnixAddress |
    PostgreConnectionParameter |
    FileSystemUsageArguments): string =
  `$`(toPreserves(x))

proc encode*(x: Tcp | DnsResolverDetail | XmlTranslatorArguments | PostgreStep |
    TcpAddress |
    XsltArguments |
    JsonSocketTranslatorStepDetail |
    PulseDetail |
    Base64DecoderArguments |
    FileSystemDetail |
    PrinterStep |
    FileSystemStep |
    CacheArguments |
    JsonSocketTranslatorStep |
    SocketAddress |
    SqliteStep |
    DnsResolverStep |
    PulseStep |
    JsonTranslatorArguments |
    UnixAddress |
    PostgreConnectionParameter |
    FileSystemUsageArguments): seq[byte] =
  encode(toPreserves(x))
