
import
  preserves

type
  RoundTripTime* {.preservesRecord: "rtt".} = object
    `address`*: string
    `minimum`*: float
    `average`*: float
    `maximum`*: float
proc `$`*(x: RoundTripTime): string =
  `$`(toPreserves(x))

proc encode*(x: RoundTripTime): seq[byte] =
  encode(toPreserves(x))
