# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/json
import preserves, preserves/jsonhooks

export fromPreservesHook, toPreservesHook
  # re-export the hooks so that conversion "just works"

type
  SendJson* {.preservesRecord: "send".} = object
    data*: JsonNode
  RecvJson* {.preservesRecord: "recv".} = object
    data*: JsonNode
