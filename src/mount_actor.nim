# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

## An actor for Linux file-system mounting.

when not defined(linux):
  {.error: "this component only tested for Linux".}

import std/oserrors
import preserves, preserves/sugar
import syndicate
import ./schema/mountpoints

type BootArgs {.preservesDictionary.} = object
  dataspace: Cap

proc mount(source, target, fsType: cstring; flags: culong; data: pointer): cint {.importc, header: "<sys/mount.h>".}
  ## `mount(2)`

proc umount(target: cstring): cint {.importc, header: "<sys/mount.h>".}
  ## `umount(2)`

proc spawnMountActor*(turn: Turn; ds: Cap): Facet {.discardable.} =
  turn.withNewFacet:
    result = turn.facet
    let
      targetPat = observePattern(!Mountpoint, { @[%1]: grabLit() })
      sourcePat = observePattern(!Mountpoint, {
          @[%0]: grabLit(),
          @[%2]: grabLit(),
        })
    during(turn, ds, ?:BootArgs) do (ds: Cap):
      during(turn, ds, targetPat) do (target: string):
        during(turn, ds, sourcePat) do (source: string, fsType: string):
          linkActor(turn, "mount_actor") do (turn: Turn):
            var mountpoint = Mountpoint(
                  source: source,
                  target: target,
                  `type`: fsType,
                )
            var rc = mount(source, target, fsType, 0, nil)
            if rc == 0:
              mountpoint.status = Status(orKind: StatusKind.success)
              turn.facet.onStop do (turn: Turn):
                discard umount(target)
            else:
              mountpoint.status = Status(orKind: StatusKind.Failure)
              mountpoint.status.failure.msg = osErrorMsg(osLastError())
            discard publish(turn, ds, mountpoint)

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; ds: Cap):
      discard spawnMountActor(turn, ds)
