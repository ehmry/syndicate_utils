# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

from os import commandLineParams
import
  pkg/preserves,
  pkg/syndicate/sturdyrefs

const usage = """
mintsturdyref OID < SECRET_KEY

Mint Sturdyrefs using a sixteen-byte secret key read from stdin using OIDs
passed as command-line parameters.

Example:
	mintsturdyref '"syndicate"' < /dev/null

See:
	https://synit.org/book/operation/builtin/gatekeeper.html#sturdyrefs
	https://synit.org/book/glossary.html?highlight=oid#oid
"""

proc main =
  var oids: seq[Value]
  for p in commandLineParams():
    case p
    of "-h", "--help", "?":
      quit(usage)
    else:
      add(oids, parsePreserves p)
  if oids.len == 0:
    stderr.writeLine """using the "syndicate" OID"""
    oids.add(toPreserves "syndicate")

  var key: array[16, byte]
  case readBytes(stdin, key, 0, 16)
  of 16: discard
  of 0: stderr.writeLine "using null key"
  else: quit "expected sixteen bytes of key from stdin"

  for oid in oids:
    let sturdy = mint(key, oid)
    doAssert validate(key, sturdy)
    stdout.writeLine(sturdy)

main()
