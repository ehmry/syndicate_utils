# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  pkg/syndicate,
  pkg/syndicate/protocols/gatekeeper,
  ./schema/[config, sql],
  ./private/pkgconfig

# Avoid Sqlite3 from the standard library because it is
# only held together by wishful thinking and dlload.

{.passC: "-Wno-error=incompatible-pointer-types".}

{.passC: pkgConfig" --cflags sqlite3".}
{.passL: pkgConfig" --libs sqlite3".}

{.pragma: sqlite3h, header: "sqlite3.h".}

var
  SQLITE_VERSION_NUMBER {.importc, sqlite3h.}: cint
  SQLITE_OK {.importc, sqlite3h.}: cint
  SQLITE_ROW {.importc, sqlite3h.}: cint
  SQLITE_DONE {.importc, sqlite3h.}: cint
  SQLITE_OPEN_READONLY {.importc, sqlite3h.}: cint

const
  SQLITE_INTEGER = 1
  SQLITE_FLOAT = 2
  SQLITE_TEXT = 3
  SQLITE_BLOB = 4
  # SQLITE_NULL = 5

type
  Sqlite3 = ptr Sqlite3Obj
  Sqlite3Obj {.importc: "sqlite3".} = object
  Stmt = ptr StmtObj
  StmtObj {.importc: "sqlite3_stmt".} = object

{.pragma: importSqlite3, importc: "sqlite3_$1", sqlite3h.}

proc libversion_number: cint {.importSqlite3.}

proc open_v2(filename: cstring; ppDb: ptr Sqlite3; flags: cint; zVfs: cstring): cint {.importSqlite3.}
proc close(ds: Sqlite3): int32 {.discardable, importSqlite3.}

proc errmsg(db: Sqlite3): cstring {.importSqlite3.}

proc prepare_v2(db: Sqlite3; zSql: cstring, nByte: cint; ppStmt: ptr Stmt; pzTail: ptr cstring): cint {.importSqlite3.}

proc step(para1: Stmt): cint {.importSqlite3.}

proc column_count(stmt: Stmt): int32 {.importSqlite3.}
proc column_blob(stmt: Stmt; col: cint): pointer {.importSqlite3.}
proc column_bytes(stmt: Stmt; col: cint): cint {.importSqlite3.}
proc column_double(stmt: Stmt; col: cint): float64 {.importSqlite3.}
proc column_int64(stmt: Stmt; col: cint): int64 {.importSqlite3.}
proc column_text(stmt: Stmt; col: cint): cstring {.importSqlite3.}
proc column_type(stmt: Stmt; col: cint): cint {.importSqlite3.}
proc finalize(stmt: Stmt): cint {.importSqlite3.}

doAssert libversion_number() == SQLITE_VERSION_NUMBER

proc answer(query: Query; a: Value): QueryAnswer =
  result.query = query
  result.answer = a

proc assertError(turn: Turn; ds: Cap; db: Sqlite3; query: Query; ctx: string) =
  publish(turn, ds, query.answer(SqlError(
      msg: $errmsg(db),
      context: ctx,
    ).toPreserves))

proc extractValue(stmt: Stmt; col: cint): Value =
  case column_type(stmt, col)
  of SQLITE_INTEGER:
    result = toPreserves(column_int64(stmt, col))
  of SQLITE_FLOAT:
    result = toPreserves(column_double(stmt, col))
  of SQLITE_TEXT:
    result = Value(kind: pkString, string: newString(column_bytes(stmt, col)))
    if result.string.len > 0:
      copyMem(addr result.string[0], column_text(stmt, col), result.string.len)
  of SQLITE_BLOB:
    result = Value(kind: pkByteString, bytes: newSeq[byte](column_bytes(stmt, col)))
    if result.bytes.len > 0:
      copyMem(addr result.bytes[0], column_blob(stmt, col), result.bytes.len)
  else:
    result = initRecord("null")

proc extractTuple(stmt: Stmt; arity: cint): Value =
  result = initSequence(arity)
  for col in 0..<arity: result[col] = extractValue(stmt, col)

proc renderSql(tokens: openarray[Value]): string =
  for token in tokens:
    if result.len > 0: result.add ' '
    case token.kind
    of pkSymbol:
      result.add token.symbol.string
    of pkString:
      result.add '\''
      result.add token.string
      result.add '\''
    of pkFloat, pkRegister, pkBigInt:
      result.add $token
    of pkBoolean:
      if token.bool: result.add '1'
      else: result.add '0'
    else:
      return ""

proc bootSqliteActor(turn: Turn; path: string, observer: Cap) =
  stderr.writeLine("opening SQLite database ", path)
  var db: Sqlite3
  if open_v2(path, addr db, SQLITE_OPEN_READONLY, nil) != SQLITE_OK:
    publish(turn, observer,
        Rejected(detail: toPreserves($errmsg(db))))
  else:
    turn.facet.onStop do (turn: Turn):
      close(db)
      stderr.writeLine("closed SQLite database ", path)
    let ds = turn.facet.newDataspace()
    publish(turn, observer,
        ResolvedAccepted(responderSession: ds))

    during(turn, ds, QueryQuestion.grabTypeFlat) do (query: Query):
      var
        stmt: Stmt
        text = renderSql query.statement
      if text == "":
        publish(turn, ds, query.answer("invalid statement".toPreserves))
      elif prepare_v2(db, text, text.len.cint, addr stmt, nil) != SQLITE_OK:
        assertError(turn, ds, db, query, text)
      else:
        try:
          let arity = column_count(stmt)
          var res = step(stmt)
          while res == SQLITE_ROW:
            var rec = extractTuple(stmt, arity)
            publish(turn, ds, QueryAnswer(
                query: query,
                answer: rec))
            res = step(stmt)
          assert res != 100
          if res != SQLITE_DONE:
            assertError(turn, ds, db, query, text)
        finally:
          if finalize(stmt) != SQLITE_OK: assertError(turn, ds, db, query, text)

proc spawnSqliteActor*(turn: Turn; relay: Cap): Facet {.discardable.} =
  turn.withNewFacet:
    result = turn.facet
    let pat = Resolve?:{ 0: SqliteStep.grabTypeFlat, 1: grab() }
    during(turn, relay, pat) do (path: string, observer: Cap):
      linkActor(turn, path) do (turn: Turn):
        bootSqliteActor(turn, path, observer)

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; relay: Cap):
      spawnSqliteActor(turn, relay)
