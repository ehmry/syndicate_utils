# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

## ESC/P printer control actor.

import
  std/[cmdline, oserrors, posix, sequtils, sets],
  pkg/sys/[files, ioqueue],
  pkg/syndicate,
  pkg/syndicate/relays,
  pkg/syndicate/protocols/gatekeeper,
  ./private/esc_p

from pkg/sys/handles import FD

type
  HandleSet = HashSet[Handle]

  Printer = ref object of Entity
    device: AsyncFile
    boldHandles, italicHandles, superscriptHandles, subscriptHandles: HandleSet
    buffer: seq[byte]
    isBusy: bool

proc flush(printer: Printer) {.asyncio.} =
  printer.isBusy = true
  while printer.buffer.len > 0:
    let n = printer.device.write(printer.buffer)
    if n > 0:
      printer.buffer.delete(0..<n)
    elif n < 0:
      osLastError().osErrorMsg().quit()
  printer.isBusy = false

proc write(printer: Printer; s: string) {.inline.} =
  printer.buffer.add cast[seq[byte]](s)
  if not printer.isBusy:
    discard trampoline:
      whelp printer.flush()

proc writeLine(printer: Printer; s: string) {.inline.} =
  printer.write(s)
  printer.write("\r\n")

proc escMessage(t: Turn; e: Entity; v: Value) =
  let printer = Printer(e)
  if v.isString:
    printer.write(v.string)
      # TODO: unicode?
      # TODO: line breaks?

proc assert(printer: Printer; handles: var HandleSet; ctrl: string; h: Handle) =
  if handles.len == 0: printer.write(ctrl)
  handles.incl h

proc retract(printer: Printer; handles: var HandleSet; ctrl: string; h: Handle) =
  handles.excl h
  if handles.len == 0: printer.write(ctrl)

proc escPublish(t: Turn; e: Entity; a: Value; h: Handle) =
  let printer = Printer(e)
  if a.isRecord("bold"):
    printer.assert(printer.boldHandles, SelectBoldFont, h)

  elif a.isRecord("italic"):
    printer.assert(printer.italicHandles, SelectItalicFont, h)

  elif a.isRecord("superscript"):
    printer.assert(printer.superscriptHandles, SelectSuperScript, h)

  elif a.isRecord("subscript"):
    printer.assert(printer.subscriptHandles, SelectSubScript, h)

proc escRetract(t: Turn; e: Entity; h: Handle) =
  let printer = Printer(e)
  if printer.boldHandles.contains h:
    printer.retract(printer.boldHandles, CancelBoldFont, h)

  elif printer.italicHandles.contains h:
    printer.retract(printer.italicHandles, CanceItalicFont, h)

  elif printer.superscriptHandles.contains h:
    printer.retract(printer.superscriptHandles, CancelAltScript, h)

  elif printer.subscriptHandles.contains h:
    printer.retract(printer.subscriptHandles, CancelAltScript, h)

proc devicePath: string =
  if paramCount() < 1:
    quit "missing path to printer device file"
  if paramCount() > 1:
    quit "too many command line parameters"
  paramStr(1)

proc openPrinter(turn: Turn): Printer =
  result = Printer(
      facet: turn.facet,
      a: escPublish,
      r: escRetract,
      m: escMessage)
  result.facet = turn.facet
  let fd = posix.open(devicePath(), O_WRONLY or O_NONBLOCK, 0)
  if fd < 0: osLastError().osErrorMsg().quit()
  result.device = newAsyncFile(FD fd)
  result.write(InitializePrinter)

runActor(devicePath()) do (turn: Turn):
  let printer = openPrinter(turn)
  resolveEnvironment(turn) do (turn: Turn; relay: Cap):
    let pat = Resolve?:{0: matchRecord("printer"), 1: grab()}
    during(turn, relay, pat) do (cont: Cap):
      # Publish for any <printer> step.
      discard publish(turn, cont, ResolvedAccepted(
          responderSession: printer.newCap))
