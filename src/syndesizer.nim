# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

## Syndicate multitool.

import
  pkg/syndicate,
  pkg/syndicate/relays,
  pkg/syndicate/drivers/[timers, subprocesses]

import ./syndesizer/[
  base64_decoder,
  cache_actor,
  file_systems,
  file_system_usage,
  json_socket_translator,
  json_translator,
  pulses,
  xml_translator]

runActor("syndesizer") do (turn: Turn):
  resolveEnvironment(turn) do (turn: Turn; relay: Cap):
    let core = turn.facet.newDataspace()
    spawnBase64Decoder(turn, relay)
    spawnCacheActor(turn, core, relay)
    spawnFileSystemActor(turn, relay)
    spawnFileSystemUsageActor(turn, relay)
    gatekeepJsonSocketTranslator(turn, relay)
    spawnJsonStdioTranslator(turn, relay)
    spawnPulseActor(turn, core, relay)
    spawnSubprocessAdapterGatekeeper(turn, relay)
    spawnTimerDriver(turn, core)
    spawnXmlTranslator(turn, relay)
