{
  pkgs ? import <nixpkgs> { },
  src ? if pkgs.lib.inNixShell then null else pkgs.lib.cleanSource ./.,
}:
with pkgs;

buildNimSbom (finalAttrs: {
  inherit src;
  buildInputs = [
    getdns
    libxml2
    libxslt
    openssl
    postgresql
    sqlite
  ];
}) ./sbom.json
